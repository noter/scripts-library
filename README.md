#scripts library

建一个库，里面存放各种实用的脚本，实现部分工作自动化，提高准确率、效率。

## app_utils目录
移动应用开发相关的脚本
- make_icons.sh 一键生成不同规格的应用图标
- make_stickericons.sh 一键生成不同规格的apple的sticker图标

## sys_utils目录
系统相关脚本
- show_quota.sh 查看Mac一些可删除的文件各占多少空间
