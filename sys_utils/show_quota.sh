#!/bin/bash
#########################################
# Author: (wind)wind-wood@163.com
# Date: 2016-05-05
# Info: Mac可怜的有限空间，总是被各种生成的
# 文件占用，脚本用于显示这些占用空间的文件、
# 目录各占多少空间，方便自己判断、删除
#########################################

echo "=================================="
echo "---------- script start ----------"
echo "=================================="
echo '可重新生成的设备支持文件：'
du -hd1 ~/Library/Developer/Xcode/iOS\ DeviceSupport/

echo "=================================="
echo '归档内容，注意备份关键版本内容：'
du -hd1 ~/Library/Developer/Xcode/Archives/

echo "=================================="
echo 'Derivedata，Xcode的项目文件，运行时可能要重新生成，请权衡时间与空间：'
derived_data_dir=~/Library/Developer/Xcode/DerivedData/
for i in `ls $derived_data_dir`; 
do 
	echo "-------------" ; 
	cat $derived_data_dir/$i/info.plist | sed -n -e 's/	<string>//' -e 's/<\/string>//g p';
	du -hd0 $derived_data_dir/$i ; 
done

echo "=================================="
echo "---------- script end ------------"
echo "=================================="
