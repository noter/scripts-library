local json = require('json')
--local json = require('p_json')

local cnt = {}
cnt.images = {}
cnt.info = {
    version = 1,
    author = "xcode",
}
cnt.properties = {
    ["pre-rendered"] = true,
}

local devices = {
    ["iphone"] = {
        [1] = {29, 57 },
        [2] = {20, 29, 40, 57, 60 },
        [3] = {20, 29, 40, 60 },
    },
    ["ipad"] = {
        [1] = {20, 29, 40, 50, 72, 76 },
        [2] = {20, 29, 40, 50, 72, 76, 83.5 },
    },
    ["ios-marketing"] = {
        [1] = {1024 },
    },
}

for dev, dInfo in pairs(devices) do
    for scale, sizeList in pairs(dInfo) do
        for _, pix in ipairs(sizeList) do
            local picSize = string.format("%sx%s", pix, pix)
            local picFilename = string.format("logo%sx%s.png", pix, scale)
            local fileExists, err = io.open(picFilename)
            if not fileExists then picFilename = nil end
            local tmpTable = {
                size = picSize,
                idiom = dev,
                filename = picFilename,
                scale = string.format("%sx", scale)
            }
            table.insert(cnt.images, tmpTable)
        end
    end
end

--local outData = json:encode_pretty(cnt)
local outData = json.encode(cnt)
local outJson = assert(io.open("Contents.json", "w"))
outJson:write(outData)
outJson:close()
