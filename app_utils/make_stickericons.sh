#!/bin/bash

# filenames
input_icon_name=logo.png
output_icon_name=logo.png

if((1==$#))
then
	input_icon_name=$1
elif((2==$#))
then
	input_icon_name=$1
	output_icon_name=$2
elif((2<$#))
then
	echo "[usage]"
	echo "$0 [input [output]]"
	exit 1
fi
out_file_name=${output_icon_name%.png}

# target directories
ios_dir=ios_sticker_icons

if [ -e $ios_dir ]
then
	echo $ios_dir exists!
	exit 1
fi


to_ios()
{
	#1x2 size; 3 suffix;
	mkdir -p ${ios_dir}
	echo "ios: convert -resize $1x$2\! $input_icon_name ${ios_dir}/${out_file_name}${3}.png"
	convert -resize $1x$2\! "$input_icon_name" "${ios_dir}/${out_file_name}${3}.png"
}

#bash does not support floating point arithmetics

# 60  45      2   3
# 67  50      2   
# 74  55      2   
# 27  20      2   3
# 32  24      2   3
# 1024    768 1       

to_ios $((60*2)) $((45*2)) "60x45x2"
to_ios $((60*3)) $((45*3)) "60x45x3"

to_ios $((67*2)) $((50*2)) "67x50x2"
to_ios $((74*2)) $((55*2)) "74x55x2"
to_ios $((27*2)) $((20*2)) "27x20x2"
to_ios $((27*3)) $((20*3)) "27x20x3"
to_ios $((32*2)) $((24*2)) "32x24x2"
to_ios $((32*3)) $((24*3)) "32x24x3"
to_ios 1024 768 "1024x768x1"
