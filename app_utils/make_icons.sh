#!/bin/bash

# filenames
input_icon_name=logo.png
output_icon_name=logo.png

if((1==$#))
then
	input_icon_name=$1
elif((2==$#))
then
	input_icon_name=$1
	output_icon_name=$2
elif((2<$#))
then
	echo "[usage]"
	echo "$0 [input.png [output.png]]"
	exit 1
fi
out_file_name=${output_icon_name%.png}

# target directories
ios_dir=ios_icons
android_dir=android_icons

if [ -e $ios_dir ]
then
    rm -rf $ios_dir
	echo $ios_dir exists!
	#exit 1
fi

if [ -e $android_dir ]
then
    rm -rf $android_dir
	echo $android_dir exists!
    #exit 1
fi


to_ios()
{
	#1 size; 2 suffix;
	mkdir -p ${ios_dir}
	echo "ios: convert -resize x$1 $input_icon_name ${ios_dir}/${out_file_name}${2}.png"
	convert -resize x$1 "$input_icon_name" "${ios_dir}/${out_file_name}${2}.png"
}
to_android()
{
	#1 size; 2 target dir
	mkdir -p ${android_dir}/${2}
	echo "android: convert -resize x$1 $input_icon_name ${android_dir}/${2}/${out_file_name}.png "
	convert -resize x$1 "$input_icon_name" "${android_dir}/${2}/${out_file_name}.png"
}

#bash does not support floating point arithmetics

#ios
for i in 20 29 40 50 57 72 76 1024
do
	to_ios $i "${i}x1"
done
for i in 20 29 40 50 57 60 72 76
do
	to_ios $(($i*2)) "${i}x2"
done
for i in 20 29 40 60
do
	to_ios $(($i*3)) "${i}x3"
done
to_ios 167 "83.5x2" 

#android
to_android 36 drawable-ldpi
to_android 48 drawable-mdpi
to_android 72 drawable-hdpi
to_android 96 drawable-xhdpi


############## new handle
#cp buildIconJson.lua ios_icons/
#cp json.lua ios_icons/
#cd ios_icons
#lua buildIconJson.lua
#rm *.lua
#cd -

