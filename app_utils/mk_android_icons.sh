#!/bin/bash

input_icon_name=logo.png    # 1024px x 1024px
output_icon_name=ic_launcher.png

if((1==$#))
then
	input_icon_name=$1
elif((2==$#))
then
	input_icon_name=$1
	output_icon_name=$2
elif((2<$#))
then
	echo "[usage]"
	echo "$0 [input.png [output.png]]"
	exit 1
fi
out_file_name=${output_icon_name%.png}

android_dir=android_icons

if [ -e $android_dir ]
then
    rm -rf $android_dir
    #exit 1
fi

isFitSize=$(identify "${input_icon_name}" | grep "1024x1024")
if [[ "" == "$isFitSize" ]]; then
    echo "input src png size must be 1024x1024"
    exit 1
fi

# tmp round icon
# ref: http://www.imagemagick.org/Usage/thumbnails/
convert "$input_icon_name" -alpha set \( +clone -distort DePolar 0 -virtual-pixel HorizontalTile -background None -distort Polar 0 \) -compose Dst_In -composite -trim +repage tmp_circle.png

convert "$input_icon_name" \
	\( +clone -alpha extract \
		-draw 'fill black polygon 0,0 0,150 150,0 fill white circle 150,150 150,0' \
		\( +clone -flip \) -compose Multiply -composite \
		\( +clone -flop \) -compose Multiply -composite \
	\) -alpha off -compose CopyOpacity -composite tmp_corners.png

to_android()
{
	mkdir -p ${android_dir}/${2}
    echo "to size: $1x$1"
	convert -resize x$1 "tmp_corners.png" "${android_dir}/${2}/${out_file_name}.png"
	convert -resize x$1 "tmp_circle.png" "${android_dir}/${2}/${out_file_name}_round.png"
}

to_android 48 mipmap-mdpi
to_android 72 mipmap-hdpi
to_android 96 mipmap-xhdpi
to_android 144 mipmap-xxhdpi
to_android 192 mipmap-xxxhdpi

mkdir -p ${android_dir}/drawable
convert -resize x20 "tmp_corners.png" "${android_dir}/drawable/notification_icon.png"
convert -resize x48 "tmp_corners.png" "${android_dir}/drawable/notification_large_icon.png"

tarZip=androidIcon_`date +%Y%m%d`.zip
if [ -e $tarZip ]; then
    rm -rf $tarZip
fi
zip -r $tarZip $android_dir
rm -rf $android_dir
# sz $tarZip
