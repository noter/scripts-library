#!/bin/bash

# filenames
input_icon_name=logo.png
output_icon_name=logo.png

if((1==$#))
then
	input_icon_name=$1
elif((2==$#))
then
	input_icon_name=$1
	output_icon_name=$2
elif((2<$#))
then
	echo "[usage]"
	echo "$0 [input.png [output.png]]"
	exit 1
fi
out_file_name=${output_icon_name%.png}

# target directories
ios_dir=ios_icons

if [ -e $ios_dir ]
then
    rm -rf $ios_dir
	echo $ios_dir exists!
	#exit 1
fi


to_ios()
{
	#1 size; 2 suffix;
	mkdir -p ${ios_dir}
    echo "ios: $1x$1"
	convert -flatten -resize x$1 "$input_icon_name" "${ios_dir}/${out_file_name}${2}.png"
}

for i in 20 29 40 50 57 72 76 1024
do
	to_ios $i "${i}x1"
done
for i in 20 29 40 50 57 60 72 76
do
	to_ios $(($i*2)) "${i}x2"
done
for i in 20 29 40 60
do
	to_ios $(($i*3)) "${i}x3"
done
to_ios 167 "83.5x2" 

############## new handle
#cp buildIconJson.lua ios_icons/
#cp json.lua ios_icons/
#cd ios_icons
#lua buildIconJson.lua
#rm *.lua
#cd -

tarZip=iosIcon_`date +%Y%m%d`.zip
if [ -e $tarZip ]; then
    rm -rf $tarZip
fi
zip -r $tarZip $ios_dir
rm -rf $ios_dir
